package szoftlab3;

import java.awt.Graphics;

public class CircleDraw extends Draw {

	@Override
	public void makeDraw(Graphics g) {
		int radius = (int) Math.sqrt((startx - endx) * (startx - endx) + (starty - endy) * (starty - endy));
		g.drawOval(startx - radius, starty - radius, 2 * radius, 2 * radius);
	}

}
