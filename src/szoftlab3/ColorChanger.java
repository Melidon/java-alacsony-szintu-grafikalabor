package szoftlab3;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JColorChooser;

public class ColorChanger implements ActionListener {
	
	private Component component;
	
	public ColorChanger(Component component) {
		this.component = component;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		Color color = JColorChooser.showDialog(this.component , "Color Picker", Color.BLACK);
		if (color != null) {
			Draw.color = color;
		}

	}

}
