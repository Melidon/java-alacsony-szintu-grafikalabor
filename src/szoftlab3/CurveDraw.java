package szoftlab3;

import java.awt.Graphics;
import java.awt.event.MouseEvent;

public class CurveDraw extends Draw {

	public void makeDraw(Graphics g) {
		g.drawLine(startx, starty, endx, endy);
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		realDraw(arg0, canvas.getBottom());
		startx = endx;
		starty = endy;
	}

}